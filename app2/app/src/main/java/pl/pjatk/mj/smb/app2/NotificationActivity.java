package pl.pjatk.mj.smb.app2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import static pl.pjatk.mj.smb.app2.MyReceiver.TEXT;

public class NotificationActivity extends AppCompatActivity {
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        textView = (TextView) findViewById(R.id.notificationTextView);

        Intent intent = getIntent();
        String text = intent.getExtras().getString(TEXT);
        textView.setText(text);
    }
}
