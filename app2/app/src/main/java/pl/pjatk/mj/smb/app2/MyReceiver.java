package pl.pjatk.mj.smb.app2;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {
    public static final String TEXT = "text";
    private static int notificationId = 0;

    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String text = intent.getExtras().getString(TEXT);
        Log.i(this.getClass().toString(), text);

        Intent startActivityIntent = new Intent(context, NotificationActivity.class);
        startActivityIntent.putExtra(TEXT, text);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, startActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(context)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(true)
            .setContentText(text)
            .setContentTitle("App2 - notyfikacja")
            .setContentIntent(pendingIntent)
            .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, notification);
    }
}
