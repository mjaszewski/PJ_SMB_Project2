package pl.pjatk.mj.smb.app1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private static final String TEXT = "text";
    private static final String PERMISSION = "pl.pjatk.mj.smb.PROJECT_2_PERMISSION";
    private static final String ACTION = "pl.pjatk.mj.smb.PROJECT_2_ACTION";
    private Button button;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        editText = (EditText) findViewById(R.id.editText);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                editText.setText("");
                Intent intent = new Intent();
                intent.setAction(ACTION);
                intent.putExtra(TEXT, text);
                sendBroadcast(intent);
            }
        });
    }
}
